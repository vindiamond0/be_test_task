<?php

namespace packager;

class Packager
{
    protected $backets = [];

    protected $remain = [];

    protected $hostMachines;

    protected $virtualMachines;

    /**
     * @var AlgorithmInterface
     */
    protected $algorithm;

    /**
     * @param AlgorithmInterface $algorithm
     */
    public function setAlgorithm(AlgorithmInterface $algorithm): void
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @param mixed $hostMachines
     */
    public function setHostMachines($hostMachines): void
    {
        $this->hostMachines = $hostMachines;
    }

    /**
     * @param mixed $virtualMachines
     */
    public function setVirtualMachines($virtualMachines): void
    {
        $this->virtualMachines = $virtualMachines;
    }

    /**
     * @throws \Exception
     */
    public function process()
    {
        if (empty($this->hostMachines) || empty($this->virtualMachines)) {
            throw new \Exception('No input data');
        }

        if (empty($this->algorithm)) {
            throw new \Exception('The algorithm isn\'t specified');
        }

        $this->algorithm->process(
            $this->backets,
            $this->remain,
            $this->hostMachines,
            $this->virtualMachines);
    }

    public function getBuckets(): array
    {
        return $this->backets;
    }

    public function getRemain(): array
    {
        return $this->remain;
    }
}