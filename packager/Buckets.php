<?php

namespace packager;

class Buckets implements \Iterator, BucketInterface
{
    protected $buckets = [];

    /**
     * @var int
     */
    private $position;

    public function __construct(array $buckets)
    {
        $this->buckets = $buckets;
    }

    public function fit(int $bucketKey, array $item): bool
    {
        foreach ($item as $key => $value) {
            if (array_key_exists($key, $this->current())
                && is_numeric($this->buckets[$bucketKey][$key])
                && $this->buckets[$bucketKey][$key] < $value) {
                return false;
            }
        }

        // the item can be fit
        // reduce the capacity of the bucket
        foreach ($item as $key => $value) {
            if (is_numeric($this->buckets[$bucketKey][$key])) {
                $this->buckets[$bucketKey][$key] -= $value;
            }
        }

        return true;
    }

    public function current()
    {
        return $this->buckets[$this->position];
    }

    public function next()
    {
        $this->position++;
    }

    public function key()
    {
        return $this->position;
    }

    public function valid()
    {
        return $this->position < count($this->buckets);
    }

    public function rewind()
    {
        $this->position = 0;
    }
}