<?php

namespace packager;

interface BucketInterface
{
    public function fit(int $bucketKey, array $item): bool;
}