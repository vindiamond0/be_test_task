<?php

namespace packager;

interface AlgorithmInterface
{
    public function process(&$result, &$remain, Buckets &$buckets, array $items);
}