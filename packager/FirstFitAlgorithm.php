<?php

namespace packager;

class FirstFitAlgorithm implements AlgorithmInterface
{
    /**
     * Implementation of the algorithm
     * @param $result
     * @param $remain
     * @param Buckets $buckets
     * @param $items
     */
    public function process(&$result, &$remain, Buckets &$buckets, array $items)
    {
        foreach ($items as $itemKey => $item) {

            foreach ($buckets as $bucketKey => $bucket) {
                if ($buckets->fit($bucketKey, $item)) {
                    $result[$bucketKey][] = $itemKey;

                    continue 2;
                }
            }

            $remain[] = $itemKey;
        }
    }
}