<?php

use packager\Buckets;
use packager\FirstFitAlgorithm;
use packager\Packager;
use Symfony\Component\Yaml\Yaml;

include "vendor/autoload.php";

// host machines
$hostMachinesConfFile = 'hostMachines.yaml';

$hostMachinesData = file_exists($hostMachinesConfFile) ?
    Yaml::parseFile($hostMachinesConfFile) :
    [];

// virtual machines
$virtualMachinesConfFile = 'virtualMachines.yaml';

$virtualMachinesData = file_exists($virtualMachinesConfFile) ?
    Yaml::parseFile($virtualMachinesConfFile) :
    [];

// initialize the Packager
$packager = new Packager();

$packager->setHostMachines(new Buckets($hostMachinesData));

$packager->setVirtualMachines($virtualMachinesData);

$packager->setAlgorithm(new FirstFitAlgorithm());

try {
    $packager->process();
} catch (\Exception $e) {
    exit($e->getMessage());
}

function generateOutput($mapping, $hostMachinesData, $virtualMachinesData)
{
    foreach ($mapping as $hostMachine => $virtualMachines) {
        $result[] = [
            'HostMachine' => $hostMachinesData[$hostMachine]['name'],
            'AssignedVirtualMachines' =>
                array_map(
                    function ($item) use ($virtualMachinesData) {
                        return $virtualMachinesData[$item]['name'];
                    },
                    $virtualMachines
                )
        ];
    }

    return $result ?? [];
}

// output layout
file_put_contents('layout.yaml',
    Yaml::dump(
        generateOutput(
            $packager->getBuckets(),
            $hostMachinesData,
            $virtualMachinesData
        ),
        5
    )
);

// output virtual machines that can't be fit
file_put_contents('remain.yaml',
    Yaml::dump(
        array_map(
            function ($item) use ($virtualMachinesData) {
                return $virtualMachinesData[$item]['name'];
            },
            $packager->getRemain()
        ),
        5
    )
);
